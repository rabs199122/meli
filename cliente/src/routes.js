import React from 'react';
import SearchNav  from './components/search-nav/searchNav';
import { Home } from './views/Home/home';
import Items from './views/Items/items';
import SingleItem from './views/single-item/singleItem';
import { NoMatch } from './views/no-match/noMatch';
import { Route, Switch } from 'react-router-dom';

export const Routes = () =>{
    return (
        <div id="app" className="loader">
            <SearchNav />
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/items"  component={Items} />
                <Route exact path="/items/:id"  component={SingleItem} />
                <Route component={NoMatch} />
            </Switch>
        </div>
    )
}

