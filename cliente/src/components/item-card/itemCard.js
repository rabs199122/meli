import React from 'react';
import './itemCard.scss'


export const ItemCard = function({item}) {
   



    let {picture, title, description, sold_quantity, price} = item;
    let {amount, currency} = price;

    currency = currency === 'ARS' ? '$' : currency;
    let condition = item.condition === 'new' ? 'Nuevo' : 'usado';
    let itemElement = (
        <section id="item-container">
            <div className="item-product">
                <figure className="item-image">
                    <img src={picture} alt={title} />
                </figure>
                <section className="item-description">
                    <p className="description-title">
                        Descripcion del production
                    </p>
                    <p className="description">
                        {description}
                    </p>
                </section>
            </div>
            <div className="item-info">
                <div className="item-condition">{condition} - {sold_quantity} vendidos </div>
                <header>
                    <div className="item-title">{title}</div>
                </header>
                <div className="item-price">{currency} {amount}</div>
                <input type="submit" className="item-buy-button primary" value="comprar" />
            </div>
            
            {/* <p>{state.item.item.condition}</p>   */}

        </section>
    )
    return (
        <div>
            {itemElement}
        </div>
        
    );
};

