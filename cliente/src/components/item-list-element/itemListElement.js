import React from 'react';
import './itemListElement.scss'
import {Link} from 'react-router-dom'; 


export const ItemListElement = function({item}) {
    console.log(item);

    const free_shipping = !item.free_shipping ? 'none' : 'inline-block';

    let {id,picture, title, price} = item;
    let {amount, currency} = price;

    currency = currency === 'ARS' ? '$' : currency;

    let styles = {
        shippingCircle : {
            display: free_shipping
        }
    }

    let condition = item.condition === 'new' ? 'Nuevo' : 'usado';
    let itemElement = (
        <Link  to={`/items/${id}`}>
            <div id="item-element">
                <div className="item-element-image">
                    <figure>
                        <img src={picture} alt={title} />                   
                    </figure>
                </div>
                <div className="item-element-info">
                    <div className="item-element-details">
                        <div className="item-price">
                            <span className="currency">{currency} </span>
                            <span className="price">{amount} </span>
                            <span className="shipping-circle" style={styles.shippingCircle}>{item.free_shipping} </span>
                        </div>
                        <div className="item-element-title">
                            <p>
                                {title}  
                            </p>
                        </div> 
                    </div>
                    <div className="item-element-usage">
                        {condition}
                    </div>
                </div>
            </div>
        </Link>
    )
    return itemElement;
};

