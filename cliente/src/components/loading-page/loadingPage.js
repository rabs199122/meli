import React from 'react';
import '../not-found-items/noFoundItems.scss'


export const Loading = function() {
 
    let loading = (
        <section id="not-found" className="container">
            <p>
                CARGANDO
            </p> 

        </section>
    )
    return (
        <div>
            {loading}
        </div>
    );
};

