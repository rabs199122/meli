import React from 'react';
import './noFoundItems.scss';


export const NotFoundItems = function() {
 
    let notFoundElement = (
        <section id="not-found" className="container">
            <p>
                No hay publicaciones que coincidan con tu búsqueda.
            </p> 

        </section>
    )
    return (
        <div>
            {notFoundElement}
        </div>
    );
};

