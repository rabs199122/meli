import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import logo from '../../assets/logo.png';
import search from '../../assets/search.png';
import './searchNav.scss';

class SearchNav extends Component {
    
    render(){
        // console.log(this.props);
        
        return (
            <div id="search-container">
                <div className="container">
                    <figure className="logo-icon">
                        <img src={logo}  alt="logo" />
                    </figure>
                    <form method="get" className="search-form" action="/items">
                        <input className="search-input" type="text" name="search" placeholder="  ¿Qué estás buscando?"></input>
                        <button className="search-button" type="submit">
                            <img src={search}  alt="icon"/>
                        </button>
                    </form>
                </div>
            </div>
        )
    }
}


export default withRouter(SearchNav);

