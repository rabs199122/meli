import React from 'react';
import './relateCategoriesList.scss'

export const RelatedCategoriesList = function({categories}) {

    let categoriesListElements = categories.map(category =>
        <li key={category} className="relate-categories-list-item">
             {category}<span className="category-separator"> ></span>
        </li>
    );

    let categoriesList = <ul id="related-category-list">{categoriesListElements}</ul>;
    
    return categoriesList;
};

