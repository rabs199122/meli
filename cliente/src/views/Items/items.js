import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import queryString from 'query-string'
import './items.scss';
import '../../styles.scss';

import {RelatedCategoriesList} from '../../components/related-categories-list/relateCategoriesList';
import {ItemListElement} from '../../components/item-list-element/itemListElement'
import {NotFoundItems} from '../../components/not-found-items/noFoundItems';
import {Loading} from '../../components/loading-page/loadingPage'


class Items extends Component {
    

    state = {
        items : [],
        categories : [],
        loading: true
    }

    componentDidMount() {
        const itemToSearch = queryString.parse(this.props.location.search)
        fetch(`/api/items?q=${itemToSearch.search}`)
            .then(response => response.json()).then(data => {
                this.setState({ items: data.items, categories:data.categories, loading:false })
            }).catch(err => console.log('error',err))
    }

    render() {
        const { loading } = this.state;
        let itemsList;
    
        if(loading) { 
            return <Loading />;        
        };
        
        if (this.state.items.length > 0) {
            let items = this.state.items.map(item =>{
                return <ItemListElement item={item} key={item.id}  />
            })
            itemsList = (
                <div className="container" id="items-list-view">
                
                    <RelatedCategoriesList categories={this.state.categories} />
                    <div id="items-list-container">{items}</div>
                </div>
            )
        } else {
            return <NotFoundItems />
        }

        // console.log(this.items);
        // let isNotValidRoute = t his.props.location.path

        return (
            itemsList
        )
    }
}


export default withRouter(Items);

