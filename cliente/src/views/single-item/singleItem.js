import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import { ItemCard } from '../../components/item-card/itemCard';
import {Loading} from '../../components/loading-page/loadingPage';


class SingleItem extends Component {

    
    state = {
        item : {
            price: {
                amount: '',
                currency : '',
                decimals :''
            }
        },
        loading : true
    }

    componentDidMount() {
        
        const itemId = this.props.match.params.id;
        const urlToRequest = `/api/items/${itemId}`;

        fetch(urlToRequest)
            .then(response => response.json()).then(
                data => { this.setState({ item: data.item, loading:false})
            }).catch(err => console.log('error',err))
    }

    render() {

        let {item} = this.state;
        const { loading } = this.state;

        
        if(loading) { 
            return <Loading />;        
        };
        

        let itemCard = <ItemCard item={item} key={item.id}  />

      
        return (
            <div className="container" id="items-list-view">
                <div id="items-list-container">{itemCard}</div>
            </div>
        )
    }
}


export default withRouter(SingleItem);

