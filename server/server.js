const express = require('express');
const app = express();
const port = process.env.PORT || 5000;
const bodyParser = require('body-parser');
const routes = require('./routes');

// console.log that your server is up and running
app.use(express.json());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// console.log(routes);
app.use('/api', routes);


app.get('/', (req, res) => res.send('App is working'));
app.get('*', function(req, res){
    res.send('what???', 404);
});
  
app.listen(port, () => console.log(`Listening on port ${port}`));