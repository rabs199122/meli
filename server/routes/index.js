const express = require('express')
 
const { 
    getItemsListSearchController,
    getSingleItemController
} = require('../controllers')
 
const router = express.Router()
 
router.get('/items', getItemsListSearchController)
router.get('/items/:id', getSingleItemController)
 
module.exports = router