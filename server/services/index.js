const axios = require('axios');


const getDecimals = (numberToConver) => {
    let decimals;
    let splitedNumberArray;

    if (numberToConver > 0){
        splitedNumberArray = (numberToConver + "").split(".");
        if (splitedNumberArray.length == 1) {
            splitedNumberArray.push("0");
        };
        decimals = parseInt(splitedNumberArray[1]);
    } else {
        decimals = 0;
    }
    return decimals;
};


const getFormatedCategoriesFromApi = (categoriesFromApi) => {
   
    let categories = [];
    categories = categoriesFromApi.map((category) =>{
        return category.name;
    })
    return categories;

};

const getFormatedItemsFromApi = (itemsFromApi) => {

    let items = [];
    items = itemsFromApi.map((result) => {
        let { id, title, condition, thumbnail : picture, currency_id:currency } = result;
        let { free_shipping }  = result.shipping;

        let amount = result.price == null ? 0 : result.price; 
       
        let decimals = getDecimals(amount);
        return {id, title, price:{currency, amount, decimals}, picture, condition, free_shipping};
    });
    return items;

};

const getItemsListSearch = async (author, queryParam) => {
 
    let items = [];
    let categories = [];
    let itemsFromApi;
    let categoriesFromApi;
    let urlToRequest = `https://api.mercadolibre.com/sites/MLA/search?q=${queryParam}&limit=4`;

    try {

        let list = await axios.get(urlToRequest);
       

        let existItems = list.data.results.length > 0;
        let existCateogries = list.data.filters.length > 0;

        if (existItems) {
            itemsFromApi = list.data.results;
            items = getFormatedItemsFromApi(itemsFromApi);
        } else {
            return {author, items, categories};
        };
        
        if (existCateogries) {
            categoriesFromApi = list.data.filters[0].values[0]["path_from_root"];
            categories = getFormatedCategoriesFromApi(categoriesFromApi);
        };

        return {author, items, categories};
    } catch(error){

        return error;

    };
};

const formatSingleItemFromApi = (item, description) => {

    let { id, title, sold_quantity, condition, currency_id:currency, price:amount, thumbnail:picture } = item;
    let { free_shipping }  = item.shipping;
    let decimals = getDecimals(amount);
    return {id, title, price: {currency, amount,decimals}, picture,condition,free_shipping,sold_quantity, description}

}

const getSingleItem = async (author, queryParam) => {

    let itemFromApi;
    let itemDescription;
    let descriptionFromApi;
    let item;
    let urlToGetItem = `https://api.mercadolibre.com/items/${queryParam}`;
   
    let urlToGetItemDescription = `${urlToGetItem}/description`;
    
    try {

        itemFromApi = await axios.get(urlToGetItem);
        itemDescription = await axios.get(urlToGetItemDescription);
        descriptionFromApi = itemDescription.data.plain_text;
        item = formatSingleItemFromApi(itemFromApi.data, descriptionFromApi);

        return {author, item};

    } catch(error){

        return error;

    };
};
 
module.exports = {
    getItemsListSearch,
    getSingleItem
}
 