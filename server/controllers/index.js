
const { 
	getItemsListSearch,
	getSingleItem 
} = require('../services');
 
const author = {
	"name": "Roberto",
	"lastname" : "Barrios",
};        


const getItemsListSearchController = async (req, res, next) => {
	
	const paramToSearch = req.query.q;
    try {

		const itemsLists  = await getItemsListSearch(author, paramToSearch);
		res.send(itemsLists);

  	} catch(e) {

		console.log(e.message, 'error');
		res.sendStatus(500) && next(e);

  	};
};

const getSingleItemController = async (req,res,next) => {

	const paramIdToSearch = req.params.id;

	try {

		const sigleItem  = await getSingleItem(author, paramIdToSearch);
		res.send(sigleItem);

  	} catch(e) {
		  
		console.log(e.message, 'error');
		res.sendStatus(500) && next(e);

  	};

};
 
module.exports = {
	getItemsListSearchController,
	getSingleItemController
};
 